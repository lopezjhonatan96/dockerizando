FROM python:3.9.2
ENV PYTHONUNBUFFERED 1
RUN mkdir /sistemas
WORKDIR /sistemas
COPY apiFileUpload/requirements.txt /sistemas/requirements.txt
RUN pip install -r requirements.txt
COPY /apiFileUpload /sistemas/
#################################
